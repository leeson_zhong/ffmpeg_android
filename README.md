# 1.环境

- Windows10的linux子系统

- FFmpeg-n3.4.7

- x264-master

- android-ndk-r14b

# 2.如果windows10还没使用linux子系统，快来跟着我教程来试试

- 设置开发者人员模式

  <img src=".\ffmpeg01.png" alt="ffmpeg01" style="zoom:50%;" />

  <img src=".\ffmpeg02.png" alt="ffmpeg02" style="zoom: 25%;" />

  <img src=".\ffmpeg03.png" alt="ffmpeg03" style="zoom:50%;" />

- 在windows功能那里启用适用于windows的linux子系统

  <img src=".\ffmpeg04.png" alt="ffmpeg04" style="zoom: 25%;" />

  <img src=".\ffmpeg05.png" alt="ffmpeg05" style="zoom: 25%;" />

  <img src="ffmpeg06.png" alt="ffmpeg06" style="zoom:50%;" />

- 到microsoft store下载安装ubuntu（可能有显示你可以在 Xbox One 主机上购买。(你所在的地区不支持通过 microsoft.com 购买。),但是没关系，继续点击获取进行下载。）。下载完成后右下角弹窗提示，选择启动，然后输入用户名和密码来初始化

  <img src="ffmpeg07.png" alt="ffmpeg07" style="zoom: 33%;" />

- 启动windows命令行（比如win+r，输入cmd确定），输入bash回车，进入linux

  ![ffmpeg08](ffmpeg08.png)

- 怎么安装cmake。第一次使用ubuntu，要先用sudo apt-get update来更新软件源库。更新完后使用sudo apt-get install cmake来安装。如果提示[y/NO]时，输入y继续
- 怎么切换到其它盘目录。看命令行可以在知道c盘挂在mnt目录下，比如要切换到d:/test目录，输入 cd /mnt/d/test就可以了

# 3.下载ffmpeg源代码和ndk

- 从官网或其它地方下载ffmpeg代码。我是从github上下载。听说4的版本不支持mediacodec硬解码，所以我决定下载3.4.7版本，有时候看不到releases选项，直接打开地址https://github.com/FFmpeg/FFmpeg/releases。

  ![ffmpeg09](ffmpeg09.png)

- 也可以通过git下载，如果是github的最好挂上代理，可以尝试国内的gitee

  ```
  //如果有代理，挂上代理，比如代理端口是10809
  export http_proxy=http://127.0.0.1:10809;export https_proxy=http://127.0.0.1:10809
  //指定版本，比如4.2版本
  git clone https://github.com/FFmpeg/FFmpeg.git --branch release/4.2
  ```

  

- 从官网或其它地方下载ndk。我是从官网https://developer.android.google.cn/ndk/downloads/older_releases.html下载。好像如果用高版本ndk编译的库，在低版本ndk中无法使用。所以我选择下载android-ndk-r14b-linux-x86_64.zip

  <img src="ffmpeg10.png" alt="ffmpeg10" style="zoom:50%;" />

- 可以进入FFmpeg的解压目录输入命令可以看ffmpeg支持情况
  查看所有编译配置选项：`./configure --help`
  查看支持的解码器：`./configure --list-decoders`
  查看支持的编码器：`./configure --list-encoders`
  查看支持的硬件加速：`./configure --list-hwaccels`

  <img src="ffmpeg11.png" alt="ffmpeg11" style="zoom: 50%;" />

- 如果需要使用ffmpeg来进行x264编码，还需要下载x264源码进行编译。https://code.videolan.org/videolan/x264可以下载。有stable和master两种下载。我决定下载master来试试新版x264

  <img src="ffmpeg12.png" alt="ffmpeg12" style="zoom: 33%;" />

# 4.编译FFMPEG库

- 解压下载的文件

  <img src="ffmpeg13.png" alt="ffmpeg13" style="zoom:50%;" />

- 进入x264的目录，编写build.sh。注意（NDK=写NDK解压的目录，最好是在linux的vi下写，否则可能会出现代码格式出错。有个小技巧，在windows下写好保存，在linux命令行上输入sed -e 's/^M/\n/g' build.sh和sed -i 's/\r$//' build.sh命令去掉多余符号）。本来我想把android-21改成android-17，但是发现只有21以后才有arm64

  ```
  #!/bin/bash
  NDK=/mnt/d/linux/android-ndk-r14b
  
  configure()
  {
      CPU=$1
      PREFIX=$(pwd)/android/$CPU
      HOST=""
      CROSS_PREFIX=""
      SYSROOT=""
      if [ "$CPU" == "armv7-a" ]
      then
          HOST=arm-linux
          SYSROOT=$NDK/platforms/android-21/arch-arm/
          CROSS_PREFIX=$NDK/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-
      else
          HOST=aarch64-linux
          SYSROOT=$NDK/platforms/android-21/arch-arm64/
          CROSS_PREFIX=$NDK/toolchains/aarch64-linux-android-4.9/prebuilt/linux-x86_64/bin/aarch64-linux-android-
      fi
      ./configure \
      --prefix=$PREFIX \
      --host=$HOST \
      --enable-pic \
      --enable-static \
      --enable-neon \
      --extra-cflags="-fPIE -pie" \
      --extra-ldflags="-fPIE -pie" \
      --cross-prefix=$CROSS_PREFIX \
      --sysroot=$SYSROOT
  }
  
  build()
  {
      make clean
      cpu=$1
      echo "build $cpu"
  
      configure $cpu
      #-j<CPU核心数>
      make -j4
      make install
  }
  
  build arm64
  build armv7-a
  ```

- 进入x264目录，输入./build.sh运行脚本。运行完成后，可以看到目录下有android文件夹，里面有arm64和armv7-a目录，都有.a库和include头文件

  ![ffmpeg14](ffmpeg14.png)

  ![ffmpeg15](ffmpeg15.png)

  ![ffmpeg16](ffmpeg16.png)

- 关于修改ffmpeg的configure。网上说要修改，我没有修改，发现编译出来的文件没有问题。

  ![ffmpeg17](ffmpeg17.png)

- 进入FFmpeg的目录，编写build.sh。注意（NDK=写NDK解压的目录，x264=写x264目录。最好是在linux的vi下写，否则可能会出现代码格式出错。有个小技巧，在windows下写好保存，在linux命令行上输入sed -e 's/^M/\n/g' build.sh和sed -i 's/\r$//' build.sh命令去掉多余符号）

  ```
  #!/bin/bash
  NDK=/mnt/d/linux/android-ndk-r14b
  ADDI_CFLAGS="-fPIE -pie"
  ADDI_LDFLAGS="-fPIE -pie"
  
  configure()
  {
      CPU=$1
      PREFIX=$(pwd)/android/$CPU
      x264=$(pwd)/../x264-master/android/$CPU
      HOST=""
      CROSS_PREFIX=""
      SYSROOT=""
      ARCH=""
      if [ "$CPU" == "armv7-a" ]
      then
          ARCH="arm"
          HOST=arm-linux
          SYSROOT=$NDK/platforms/android-21/arch-arm/
          CROSS_PREFIX=$NDK/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-
      else
          ARCH="aarch64"
          HOST=aarch64-linux
          SYSROOT=$NDK/platforms/android-21/arch-arm64/
          CROSS_PREFIX=$NDK/toolchains/aarch64-linux-android-4.9/prebuilt/linux-x86_64/bin/aarch64-linux-android-
      fi
      ./configure \
      --prefix=$PREFIX \
      --disable-encoders \
      --disable-decoders \
      --disable-avdevice \
      --disable-static \
      --disable-doc \
      --disable-ffplay \
      --disable-doc \
      --disable-symver \
      --enable-neon \
      --enable-shared \
      --enable-libx264 \
      --enable-gpl \
      --enable-pic \
      --enable-jni \
      --enable-pthreads \
      --enable-mediacodec \
      --enable-encoder=aac \
      --enable-encoder=gif \
      --enable-encoder=libopenjpeg \
      --enable-encoder=libmp3lame \
      --enable-encoder=libwavpack \
      --enable-encoder=libx264 \
      --enable-encoder=mpeg4 \
      --enable-encoder=pcm_s16le \
      --enable-encoder=png \
      --enable-encoder=srt \
      --enable-encoder=subrip \
      --enable-encoder=yuv4 \
      --enable-encoder=text \
      --enable-decoder=aac \
      --enable-decoder=aac_latm \
      --enable-decoder=libopenjpeg \
      --enable-decoder=mp3 \
      --enable-decoder=mpeg4_mediacodec \
      --enable-decoder=pcm_s16le \
      --enable-decoder=flac \
      --enable-decoder=flv \
      --enable-decoder=gif \
      --enable-decoder=png \
      --enable-decoder=srt \
      --enable-decoder=xsub \
      --enable-decoder=yuv4 \
      --enable-decoder=vp8_mediacodec \
      --enable-decoder=h264_mediacodec \
      --enable-decoder=hevc_mediacodec \
      --enable-hwaccel=h264_mediacodec \
      --enable-hwaccel=mpeg4_mediacodec \
      --enable-ffmpeg \
      --enable-bsf=aac_adtstoasc \
      --enable-bsf=h264_mp4toannexb \
      --enable-bsf=hevc_mp4toannexb \
      --enable-bsf=mpeg4_unpack_bframes \
      --enable-cross-compile \
      --cross-prefix=$CROSS_PREFIX \
      --target-os=android \
      --arch=$ARCH \
      --sysroot=$SYSROOT \
      --extra-cflags="-I$x264/include $ADDI_CFLAGS" \
      --extra-ldflags="-L$x264/lib"
  }
  
  build()
  {
      make clean
      cpu=$1
      echo "build $cpu"
      
      configure $cpu
      make -j4
      make install
  }
  
  build arm64
  build armv7-a
  ```
  
- 我后来编译4.2版本时发现会报错，有些makefile无法找到，需要先configure

  ```
./configure --disable-x86asm 
  ```
  
- 我后来编译4.2版本时发现会报错，install: cannot create regular file '/usr/local/share/man/man1/ffmpeg.1': Permission denied

  ```
sudo ./build.sh
  ```
  
  
  
- 我后来编译4.2版本时发现没有生产so文件，发现开始有报错aarch64-linux-android-clang is unable to create an executable file. C compiler test failed.。按照网上教程运行/build/tools/make_standalone_toolchain.py生成aarch64-linux-android-clang拷贝过去也不行。然后我下载比较新的NDK，比如r21e，但是还是报错。后来发现要修改脚本，脚本参考[CaiJingLong/build_android.sh](https://gist.github.com/CaiJingLong/feefa3b63e5b94f5ad1807825861e0e0)，我下载的r19C和r21e有clang，但r18b和之前版本没有。

  ```
#!/bin/bash
  NDK=/mnt/d/linux/android-ndk-r19c
  TOOLCHAIN=$NDK/toolchains/llvm/prebuilt/linux-x86_64
  API=21
  ADDI_CFLAGS="-fPIE -pie"
  ADDI_LDFLAGS="-fPIE -pie"
  
  configure()
  {
      CPU=$1
      PREFIX=$(pwd)/android/$CPU
      x264=$(pwd)/../x264-master/android/$CPU
      CROSS_PREFIX=""
      SYSROOT=""
      ARCH=""
      if [ "$CPU" == "armv7-a" ]
      then
          ARCH="arm"
          TOOL_CPU_NAME=armv7a
          SYSROOT=$NDK/toolchains/llvm/prebuilt/linux-x86_64/sysroot
          TOOL_PREFIX=$TOOLCHAIN/bin/$TOOL_CPU_NAME-linux-androideabi
          CC=$TOOL_PREFIX$API-clang
          CXX=$TOOL_PREFIX$API-clang++
          CROSS_PREFIX=$TOOLCHAIN/bin/arm-linux-androideabi-
      else
          ARCH="arm64"
          TOOL_CPU_NAME=aarch64
          SYSROOT=$NDK/toolchains/llvm/prebuilt/linux-x86_64/sysroot
          TOOL_PREFIX=$TOOLCHAIN/bin/$TOOL_CPU_NAME-linux-android
          CC=$TOOL_PREFIX$API-clang
          CXX=$TOOL_PREFIX$API-clang++
          CROSS_PREFIX=$TOOLCHAIN/bin/aarch64-linux-android-
      fi
      ./configure \
      --prefix=$PREFIX \
      --disable-avdevice \
      --disable-static \
      --disable-doc \
      --disable-ffplay \
      --disable-doc \
      --disable-symver \
      --enable-neon \
      --enable-shared \
      --enable-libx264 \
      --enable-gpl \
      --enable-pic \
      --enable-jni \
      --enable-pthreads \
      --enable-mediacodec \
      --enable-encoder=aac \
      --enable-encoder=gif \
      --enable-encoder=libopenjpeg \
      --enable-encoder=libmp3lame \
      --enable-encoder=libwavpack \
      --enable-encoder=libx264 \
      --enable-encoder=mpeg4 \
      --enable-encoder=pcm_s16le \
      --enable-encoder=png \
      --enable-encoder=srt \
      --enable-encoder=subrip \
      --enable-encoder=yuv4 \
      --enable-encoder=text \
      --enable-decoder=aac \
      --enable-decoder=aac_latm \
      --enable-decoder=libopenjpeg \
      --enable-decoder=mp3 \
      --enable-decoder=mpeg4_mediacodec \
      --enable-decoder=pcm_s16le \
      --enable-decoder=flac \
      --enable-decoder=flv \
      --enable-decoder=gif \
      --enable-decoder=png \
      --enable-decoder=srt \
      --enable-decoder=xsub \
      --enable-decoder=yuv4 \
      --enable-decoder=vp8_mediacodec \
      --enable-decoder=h264_mediacodec \
      --enable-decoder=hevc_mediacodec \
      --enable-hwaccel=h264_mediacodec \
      --enable-hwaccel=mpeg4_mediacodec \
      --enable-ffmpeg \
      --enable-bsf=aac_adtstoasc \
      --enable-bsf=h264_mp4toannexb \
      --enable-bsf=hevc_mp4toannexb \
      --enable-bsf=mpeg4_unpack_bframes \
      --enable-cross-compile \
      --cross-prefix=$CROSS_PREFIX \
      --target-os=android \
      --arch=$ARCH \
      --cc=$CC \
      --cxx=$CXX \
      --sysroot=$SYSROOT \
      --extra-cflags="-I$x264/include $ADDI_CFLAGS" \
      --extra-ldflags="-L$x264/lib"
  }
  
  build()
  {
      make clean
      cpu=$1
      echo "build $cpu"
      
      configure $cpu
      make -j4
      make install
  }
  
  build arm64
  build armv7-a
  
  ```
  
  
  
- 进入ffmpeg目录，输入./build.sh运行脚本。运行完成后，可以看到目录下有android文件夹，里面有arm64和armv7-a目录，都有.a库和include头文件

  ![ffmpeg18](ffmpeg18.png)

  ![ffmpeg19](ffmpeg19.png)

# 5.使用ffmpeg库

- 新建工程进行测试使用。好像android studio支持cmake，而且推荐使用。所以这次我打算用cmake来测试。（旧版android studio是勾选**Include C++ Support**，新版android studio是选择native C++）

  <img src="ffmpeg20.png" alt="ffmpeg20" style="zoom:50%;" />

- 建完工程，可以看cmake的几个关键文件和目录

  <img src="ffmpeg21.png" alt="ffmpeg21" style="zoom:50%;" />

  <img src="ffmpeg22.png" alt="ffmpeg22" style="zoom:50%;" />

- 对工程进行简单设置，把编译架构改成只支持armeabi-v7a和arm64-v8a，然后编译运行确定工程没有问题。（这里我有遇到问题，因为我原来是把工程放到android project文件夹中，结果编译时clean命令报错。网上查了下，把android project的中间空格去掉，重新打开工程编译没有问题了。原本想设置生成的so动态库最后输出的路径，但是发现有问题，这后面会提一下）

  ```
  externalNativeBuild {
      cmake {
          cppFlags ""
          abiFilters "armeabi-v7a" , "arm64-v8a"
      }
  }
  
  ```

  <img src="ffmpeg23.png" alt="ffmpeg23" style="zoom:50%;" />

- 注意如果只点三角形按钮直接运行到手机上，可能只编译了一个架构的库，比如arm64-v8a，在build选项那里选择make Module ‘app’，则可以看到app\build\intermediates\transforms\mergeJniLibs\debug\0\lib那里有多个架构的库

  <img src="ffmpeg24.png" alt="ffmpeg24" style="zoom:50%;" />

- 成功运行会在手机上看到Hello from C++（第一次运行失败，显示有个文件被占用，重新运行成功）

  <img src="ffmpeg25.png" alt="ffmpeg25" style="zoom:33%;" />

- 现在开始加点小细节，把ffmpeg库加进去编译运行。把ffmpeg目录下的库拷贝到android工程的jniLibs对应目录下。把android/armv7-a/lib的so文件拷贝到jniLibs/armeabi-v7a，把android/arm64/lib的so文件拷贝到jniLibs/arm64-v8a。（如果app/src/main没有jniLibs目录，新新建一个，jniLibs目录下如果没有armeabi-v7a和arm64-v8a目录也新建一个）

  <img src="ffmpeg26.png" alt="ffmpeg26" style="zoom:50%;" />

- 把ffmpeg目录下把头文件拷贝到工程中。在android/arm64或android/armv7-a目录下把include文件夹拷贝到工程的cpp目录下

  <img src="ffmpeg27.png" alt="ffmpeg27" style="zoom:50%;" />

- 在cmake的文件上写上头文件路径和ffmpeg的so库路径，${CMAKE_SOURCE_DIR}为CMakeLists.text所在的目录，注意相对路径。

  ```
  #设置include头文件的路径
  include_directories(
          ${CMAKE_SOURCE_DIR}/include
  )
  
  #设置lib_src_DIR指向jniLibs的对应架构目录
  set(lib_src_DIR ${CMAKE_SOURCE_DIR}/../jniLibs/${ANDROID_ABI})
  
  #导入libavcodec.so,库名为avcodec-lib
  add_library(avcodec-lib SHARED IMPORTED)
  set_target_properties(avcodec-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libavcodec.so)
  
  #导入libavfilter.so,库名为avfilter-lib
  add_library(avfilter-lib SHARED IMPORTED)
  set_target_properties(avfilter-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libavfilter.so)
  
  #导入libavformat.so,库名为avformat-lib
  add_library(avformat-lib SHARED IMPORTED)
  set_target_properties(avformat-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libavformat.so)
  
  #导入libavutil.so,库名为avutil-lib
  add_library(avutil-lib SHARED IMPORTED)
  set_target_properties(avutil-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libavutil.so)
  
  #导入libpostproc.so,库名为postproc-lib
  add_library(postproc-lib SHARED IMPORTED)
  set_target_properties(postproc-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libpostproc.so)
  
  #导入libswresample.so,库名为swresample-lib
  add_library(swresample-lib SHARED IMPORTED)
  set_target_properties(swresample-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libswresample.so)
  
  #导入libswscale.so,库名为swscale-lib
  add_library(swscale-lib SHARED IMPORTED)
  set_target_properties(swscale-lib PROPERTIES IMPORTED_LOCATION
                        ${lib_src_DIR}/libswscale.so)
  ```

  ```
  target_link_libraries( # Specifies the target library.
                         native-lib
  
                         # Links the target library to the log library
                         # included in the NDK.
                         ${log-lib}
                          avcodec-lib
                          avfilter-lib
                          avformat-lib
                          avutil-lib
                          postproc-lib
                          swresample-lib
                          swscale-lib)
  ```

- 在native-lib.c文件调用一下ffmpeg的方法，如果编译运行后没报错，那就成功了

  <img src="ffmpeg28.png" alt="ffmpeg28" style="zoom:50%;" />

- 关于设置库输出路径，我遇到两个问题，其中一个问题找不到解决方法。第一个问题，设置输出目录到jniLibs下了，编译运行时报错有多个库存在，因为so库在libs目录下和jniLibs目录下的冲突，所以要设置sourceSets指定lib目录只有jniLibs。第二个问题，引用ffmpeg库时，编译运行报错找不到libavcodec.so文件，找不到解决方法

  <img src="ffmpeg29.png" alt="ffmpeg29" style="zoom:50%;" />

  <img src="ffmpeg30.png" alt="ffmpeg30" style="zoom:50%;" />

# 6.解码视频并播放

- 继续在第5步的ffmpeg工程上继续开发。首先在native-lib.c文件上写好解码代码，然后在java上调用测试没问题，最后用surface来显示解码得到的画面。第一步，肯定要把log功能弄好，调试才方便

- FFmpeg提供av_log_set_callback的方法给我们，设置后可以获取到FFmpeg的打印信息。我们把ffmpeg的打印信息输出到android的logcat上

  ```
  #include <android/log.h>
  extern "C" {
  #include <libavformat/avformat.h>
  #include <libavcodec/avcodec.h>
  #include <libavutil/opt.h>
  #include <libavutil/imgutils.h>
  #include <libswscale/swscale.h>
  #include "libavcodec/jni.h"
  }
  const char* TAG = "native-lib,leeson,log";
  
  #define ALOGE(fmt, ...) __android_log_vprint(ANDROID_LOG_ERROR, TAG, fmt, ##__VA_ARGS__)
  #define ALOGI(fmt, ...) __android_log_vprint(ANDROID_LOG_INFO, TAG, fmt, ##__VA_ARGS__)
  #define ALOGD(fmt, ...) __android_log_vprint(ANDROID_LOG_DEBUG, TAG, fmt, ##__VA_ARGS__)
  #define ALOGW(fmt, ...) __android_log_vprint(ANDROID_LOG_WARN, TAG, fmt, ##__VA_ARGS__)
  #define ALOGV(fmt, ...) __android_log_vprint(ANDROID_LOG_VERBOSE, TAG, fmt, ##__VA_ARGS__)
  
  jboolean FFMPEG_ANDROID_DEBUG = 0;
  
  static void ffmpeg_android_log_callback(void *ptr, int level, const char *fmt, va_list vl){
      if (FFMPEG_ANDROID_DEBUG){
          switch(level) {
              case AV_LOG_DEBUG:
                  ALOGD(fmt, vl);
                  break;
              case AV_LOG_VERBOSE:
                  ALOGV(fmt, vl);
                  break;
              case AV_LOG_INFO:
                  ALOGI(fmt, vl);
                  break;
              case AV_LOG_WARNING:
                  ALOGW(fmt, vl);
                  break;
              case AV_LOG_ERROR:
                  ALOGE(fmt, vl);
                  break;
          }
      }
  }
  
  ```

- 调用其它ffmpeg方法前先运行下面两句代码就可以在android logcat看到ffmpeg的日志信息了。

  ```
  FFMPEG_ANDROID_DEBUG = 1;
  av_log_set_callback(ffmpeg_android_log_callback);
  ```

- 要实现一个方法，叫JNI_OnLoad,因为ffmpeg使用mediacodec硬解码时需要JVM

  ```
  extern "C"
  JNIEXPORT
  jint JNI_OnLoad(JavaVM *vm, void *res) {
      av_jni_set_java_vm(vm, res);
      // 返回jni版本
      return JNI_VERSION_1_6;
  }
  ```

- 解码播放使用surface，jni端需要用到ANativeWindow，因此cmake要添加-landroid库

  ```
  target_link_libraries( # Specifies the target library.
                         native-lib
  
                         # Links the target library to the log library
                         # included in the NDK.
                          -landroid
  ```

  

- 按照已有的方法，重新建个方法，函数命名方式是包名+类名+方法名，我这里叫playWithSurface，把视频地址通过String类型参数传进来,把Surface对象也传进来，使用GetStringUTFChars方法把string转换成char*使用

  ```
  int decodeVideo(const char * videoPath,JNIEnv *env);
  ANativeWindow *nativeWindow;
  extern "C" void
  Java_com_octant_testffmpeg_MainActivity_playWithSurface(
          JNIEnv *env,
          jobject,
          jstring videopath,
          jobject surface) {
  
      FFMPEG_ANDROID_DEBUG = 1;
      av_log_set_callback(ffmpeg_android_log_callback);
      const char *videoPath = env->GetStringUTFChars(videopath, JNI_FALSE);
      nativeWindow = ANativeWindow_fromSurface(env,surface);
      decodeVideo(videoPath,env);
  }
  ```

- 我们定义了decodeVide函数，开始解码视频。

  AVPacket存放读取视频的一个数据包

  AVFrame存放解码出来的一帧画面数据

  av_register_all以前是注册解码器之类的组件。不过听说现在没用了。

  ```
  int decodeVideo(const char * videoPath,JNIEnv *env){
      int ret;
      AVPacket packet;
      AVFrame *frame = av_frame_alloc();
      if (!frame ) {
          av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
          return -1;
      }
      av_register_all();
  ```

- 调用open_input_file来打开视频文件，并初始化一些变量。这个函数是我自己定义的

  ```
  if ((ret = open_input_file(videoPath)) < 0)
          goto end;
  ```

- 我们先转到open_input_file函数的定义。有3个变量是decodeVideo函数要用的，所以定义成全局变量。

  AVFormatContext保存视频的编码方式的上下文数据结构，比如avc、divx

  AVCodecContext保存的是要使用的编解码器上下文的数据结构

  Video_stream_index保存画面index，因为视频有声音和画面，读取的数据包根据index判断是哪种类型数据

  AVCodec保存要用的解码器

  avformat_open_input打开视频文件，提取视频编码信息到fmt_ctx变量

  avformat_find_stream_info根据传入的视频编码信息判断是否能找到流

  ```
  AVFormatContext *fmt_ctx;
  AVCodecContext *dec_ctx;
  int video_stream_index = -1;
  int open_input_file(const char *filename)
  {
      int ret;
      AVCodec *dec;
  
      if ((ret = avformat_open_input(&fmt_ctx, filename, NULL, NULL)) < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot open input file\n");
          return ret;
      }
  
      if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
          return ret;
      }
  ```

- av_find_best_stream根据传入的视频编码信息得到视频流index，保存到video_stream_index。

  得到解码器，保存到dec。网上很多人是用avcodec_find_decoder(id)l来寻找解码器或用avcodec_find_decoder_by_name("h264_mediacodec")寻找指定解码器，因为h264_mediacodec解码器和h264解码器id相同。我发现这个方法找到的解码器是硬解码器h264_mediacodec

  ```
  /* select the video stream */
      ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
      if (ret < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot find a video stream in the input file\n");
          return ret;
      }
      video_stream_index = ret;
  ```

- 创建并初始化解码器上下文的数据结构，保存到dec_ctx

  ```
      /* create decoding context */
      dec_ctx = avcodec_alloc_context3(dec);
      if (!dec_ctx)
          return AVERROR(ENOMEM);
  
      avcodec_parameters_to_context(dec_ctx, fmt_ctx->streams[video_stream_index]->codecpar);
      av_opt_set_int(dec_ctx, "refcounted_frames", 1, 0);
  
      /* init the video decoder */
      if ((ret = avcodec_open2(dec_ctx, dec, NULL)) < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
          return ret;
      }
  ```

- open_input_file函数内容已经讲完，现在转回到decode_video函数。现在开始进入读取视频并解码流程。

  av_read_frame读取一个数据包

  判断packet的index是不是video stream的

  avcodec_send_packet把packet发给解码器上下文结构体进行解码

  avcodec_receive_frame接收解码器上下文件结构体解码出来的avframe数据

  ```
      while (1) {
          if ((ret = av_read_frame(fmt_ctx, &packet)) < 0)
              break;
  
          if (packet.stream_index == video_stream_index) {
              ret = avcodec_send_packet(dec_ctx, &packet);
              if (ret < 0) {
                  av_log(NULL, AV_LOG_ERROR, "Error while sending a packet to the decoder\n");
                  break;
              }
  
              while (ret >= 0) {
                  ret = avcodec_receive_frame(dec_ctx, frame);
                  if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                      break;
                  } else if (ret < 0) {
                      av_log(NULL, AV_LOG_ERROR, "Error while receiving a frame from the decoder\n");
                      goto end;
                  }
  ```

- 得到解码的数据后，直接打印一句，说明程序能正确跑到这里。android surface显示是rgba格式，通过ANativeWindow_lock和ANativeWindow_unlockAndPost刷新surface的画面。通过sws_scale可以转换数据格式，也可以通过opencv或网上算法根据avframe的format值进行转换。最后把一些变量释放内存。

  ```
                  if (ret >= 0) {
                      av_log(NULL, AV_LOG_INFO, "decode a frame success\n");
                      
                      sws_scale(img_convert_ctx, (const uint8_t *const *) frame->data, frame->linesize, 0, dec_ctx->height, pRgbFrame->data, pRgbFrame->linesize);
  
                      ANativeWindow_lock(nativeWindow, &windowBuffer, 0);
  
                      memcpy(dst_data,pRgbBuff,dec_ctx->width*dec_ctx->height*4);
                      ANativeWindow_unlockAndPost(nativeWindow);
  
  
                      av_frame_unref(frame);
                  }
              }
          }
          av_packet_unref(&packet);
      }
      end:
      avcodec_free_context(&dec_ctx);
      avformat_close_input(&fmt_ctx);
      av_frame_free(&frame);
      return 0;
  ```

- Jni部分写完了，我们开始调用。首先弄好权限先，在AndroidManifest.xml添加权限。

  ```
  <!--写入扩展存储，向扩展卡写入数据-->
      <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"></uses-permission>
  ```

- Android6.0以上版本需要动态申请权限

  ```
          if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
              int writePermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
  
              if (writePermission != PackageManager.PERMISSION_GRANTED) {
                  // We don't have permission so prompt the user
                  String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};
  
                  int permsRequestCode = 200;
  
                  requestPermissions(perms, permsRequestCode);
              }
          }
  ```

  ```
      public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
  
          switch (permsRequestCode) {
  
              case 200:
  
                  boolean writeAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                  if (false == writeAccepted) {
                      if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                          String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};
  
                          requestPermissions(perms, permsRequestCode);
                      }
                  }
  
                  break;
          }
      }
  ```

- 权限弄好后，就开始在java上调用jni函数了。在xml加个surface控件，在activity上初始化surface并传输给jni。权限申请成功和surface created后才启动解码，否则播放视频失败。注意，根据代码，视频要记得放在对应路径上，比如把MOVI0001.mov放到手机根目录。

  ```
  <SurfaceView
          android:id="@+id/surfaceView"
          android:layout_width="match_parent"
          android:layout_height="match_parent"
          android:layout_centerInParent="true"/>
  ```

  ```
  public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback
  ```

  ```
  SurfaceView surfaceView;
  SurfaceHolder surfaceHolder;
  ```

  ```
  surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
  surfaceHolder = surfaceView.getHolder();
  
  surfaceHolder.addCallback(this);
  ```

  ```
  public native void playWithSurface(String videopath, Object surface);	
  ```

  ```
  @Override
  public void surfaceCreated(SurfaceHolder surfaceHolder) {
  surfaceCreatedFlag = true;
  }
  ```

  ```
  	Thread playVideoThread = new Thread(new Runnable() {
          @Override
          public void run() {
              while (surfaceCreatedFlag == false || writePermittedFlag == false){
                  try {
                      Thread.sleep(100);
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  }
              }
              String path = Environment.getExternalStorageDirectory().getAbsolutePath();
              File dir = new File(path);
              if (!dir.exists()) {
                  dir.mkdirs();
              }
              final String inputPath = path + "/MOVI0001.mov";
              playWithSurface(inputPath,surfaceHolder.getSurface());
          }
      });
  ```

  ```
  playVideoThread.start();
  ```


# 7.播放RTSP

- 再AndroidManifest.xml加上网络权限

  ```
      <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
      <uses-permission android:name="android.permission.INTERNET" />
      <uses-permission android:name="android.permission.WAKE_LOCK" />
      <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
  ```

- 在jni添加播放RTSP的代码。参照前面的代码，最主要的是加上`avformat_network_init`方法。

  ```
  extern "C" void
  Java_com_octant_testffmpeg_MainActivity_playRtspWithSurface(
          JNIEnv *env,
          jobject,
          jstring videopath,
          jobject surface) {
  
      FFMPEG_ANDROID_DEBUG = 1;
      av_log_set_callback(ffmpeg_android_log_callback);
      const char *videoPath = env->GetStringUTFChars(videopath, JNI_FALSE);
  
      nativeWindow = ANativeWindow_fromSurface(env,surface);
      int ret;
      AVPacket packet;
      AVFrame *frame = av_frame_alloc();
      if (!frame ) {
          av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
          return ;
      }
      av_register_all();
      avformat_network_init();
  
      SwsContext *img_convert_ctx;
      AVFrame *pRgbFrame;
      size_t rgbSize;
      uint8_t *pRgbBuff;
  
      AVDictionary* options = NULL;
      //av_dict_set(&options, "buffer_size", "1024000", 0);
      av_dict_set(&options, "max_delay", "000", 0);
      av_dict_set(&options, "stimeout", "2000", 0);  //设置超时断开连接时间
      av_dict_set(&options, "rtsp_transport", "tcp", 0);  //以udp方式打开，如果以tcp方式打开将udp替换为tcp
      AVCodec *dec;
  
      if ((ret = avformat_open_input(&fmt_ctx, videoPath, NULL,&options)) < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot open input stream,error code=%ld\n",AVERROR(ret));
          return ;
      }
  
      if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
          return;
      }
  
      /* select the video stream */
      ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
      if (ret < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot find a video stream in the input file\n");
          return ;
      }
      video_stream_index = ret;
  
      /* create decoding context */
      dec_ctx = avcodec_alloc_context3(dec);
      if (!dec_ctx)
          return ;
  
      avcodec_parameters_to_context(dec_ctx, fmt_ctx->streams[video_stream_index]->codecpar);
      av_opt_set_int(dec_ctx, "refcounted_frames", 1, 0);
     // dec_ctx->flags |= AV_CODEC_FLAG_LOW_DELAY;
      //av_opt_set(dec_ctx->priv_data, "tune", "zerolatency", 0);
  
      /* init the video decoder */
      if ((ret = avcodec_open2(dec_ctx, dec, NULL)) < 0) {
          av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
          return ;
      }
  
      img_convert_ctx = sws_getContext(dec_ctx->width, dec_ctx->height, dec_ctx->pix_fmt, dec_ctx->width, dec_ctx->height, AV_PIX_FMT_RGBA, SWS_FAST_BILINEAR, NULL, NULL, NULL);
  
      pRgbFrame = av_frame_alloc();
      rgbSize = (size_t) av_image_get_buffer_size(AV_PIX_FMT_RGBA, dec_ctx->width, dec_ctx->height, 1);
      pRgbBuff= (uint8_t *) (av_malloc(rgbSize));
      av_image_fill_arrays(pRgbFrame->data, pRgbFrame->linesize, pRgbBuff, AV_PIX_FMT_RGBA, dec_ctx->width, dec_ctx->height,1);
  
      void *addr_pixels;
      ANativeWindow_setBuffersGeometry(nativeWindow, dec_ctx->width, dec_ctx->height,
                                       WINDOW_FORMAT_RGBA_8888);
      ANativeWindow_Buffer windowBuffer;
      /* read all packets */
      while (1) {
          if ((ret = av_read_frame(fmt_ctx, &packet)) < 0)
              break;
  
          if (packet.stream_index == video_stream_index) {
              av_log(NULL, AV_LOG_INFO, "avcodec_send_packet\n");
              ret = avcodec_send_packet(dec_ctx, &packet);
              if (ret < 0) {
                  av_log(NULL, AV_LOG_ERROR, "Error while sending a packet to the decoder\n");
                  break;
              }
  
              while (ret >= 0) {
                  ret = avcodec_receive_frame(dec_ctx, frame);
                  if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                      break;
                  } else if (ret < 0) {
                      av_log(NULL, AV_LOG_ERROR, "Error while receiving a frame from the decoder\n");
                      goto end;
                  }
  
                  if (ret >= 0) {
                      av_log(NULL, AV_LOG_INFO, "decode a frame success\n");
                      //使用ffmpeg的yuv转换rgb24方法耗时太久，需要差不多100ms
                      //我改成转换成rgba,转换方法使用SWS_BILINEAR，耗时不久，要十几毫秒，如果SWS_FAST_BILINEAR好像更快一点
                      sws_scale(img_convert_ctx, (const uint8_t *const *) frame->data, frame->linesize, 0, dec_ctx->height, pRgbFrame->data, pRgbFrame->linesize);
                      av_log(NULL, AV_LOG_INFO, "sws_scale a frame success\n");
                      ANativeWindow_lock(nativeWindow, &windowBuffer, 0);
  
                      uint8_t *dst_data = static_cast<uint8_t *>(windowBuffer.bits);
  
                      memcpy(dst_data,pRgbBuff,dec_ctx->width*dec_ctx->height*4);
                      ANativeWindow_unlockAndPost(nativeWindow);
  
                      av_log(NULL, AV_LOG_INFO, "show a frame success\n");
  
                      av_frame_unref(frame);
                  }
              }
          }
          av_packet_unref(&packet);
      }
      end:
      avcodec_free_context(&dec_ctx);
      avformat_close_input(&fmt_ctx);
      av_frame_free(&frame);
  }
  ```

- java层参照前面代码，声明方法并调用，注意你可以网上下载RTSP player应用测试你的rtsp网址没错

  ```
  public native void playRtspWithSurface(String videopath, Object surface);
  ```

  ```
  playRtspWithSurface(url,surfaceHolder.getSurface());
  ```

- 无法打开rtsp，报-1330794744(Protocol not found）。找了好久，发现是FFmpeg的build.sh写了`--disable-network`，改成`--enable-network`或直接删除`--disable-network`，因为默认是有启动网络的，这网上找到的编译脚本坑了我。改完build.sh后，重新编译FFmpeg得到新的库。

- 但是发现播放RTSP实时视频流有延迟，网上查了是mediacodec硬件解码会延迟几帧作为缓存，网上没找到方法解决。尝试改成使用软解码方式，发现FFmpeg的build.sh的h264等只启动了mediacodec硬解码器，所以加上对应的软解码器。如果有软解码器，av_find_best_stream得到默认是软解码器，可以通过avcodec_find_decoder_by_name得到对应的硬解码和软件解码。比如avcodec_find_decoder_by_name("h264_mediacodec")是硬解码器，avcodec_find_decoder_by_name("h264")是软解码器。测试发现软解码播放rtsp视频确实没有硬解码的延迟那么大。

  ```
      --enable-decoder=vp8 \
      --enable-decoder=h264 \
      --enable-decoder=hevc \
      --enable-decoder=mpeg4 \
  ```

  ```
  	switch (dec->id){
  
          case AV_CODEC_ID_H264:{
              dec = avcodec_find_decoder_by_name("h264");
              break;
          }
  
          case AV_CODEC_ID_MPEG4:{
              dec = avcodec_find_decoder_by_name("mpeg4");
              break;
          }
  
          default:{
              break;
          }
  
      }
  ```

  

# 8.播放RTSP UDP花屏解决方法。

- 改成tcp，但是有些rtsp源不支持tcp

  ```
  av_dict_set(&options, "rtsp_transport", "tcp", 0);  //以tcp方式打开
  ```

- 把buffsize加大

  ```
  av_dict_set(&options, "buffer_size", "4096000", 0);
  ```

- 在ffmpeg源代码找到udp.c，应该在libavformat目录下，加大UDP_MAX_PKT_SIZE的值，重新编译ffmpeg库

  ```
  #define UDP_MAX_PKT_SIZE 65536*10
  ```

- 丢弃花屏的帧，参考网址：https://blog.csdn.net/sz76211822/article/details/87797475（ffmpeg播放rtsp视频流花屏解决办法)

  1.设全局变量    在丢包或解码出错时将全局变量置为不同的值   最后在使用的地方根据全局变量的值来判断该帧是否完整   全局变量可在ffmpeg任意.h文件中设置  我是在avcodec.h中设置的。这里我发现有一个问题，那就是编译多个动态库，而这些库都使用这个全局变量，所以编译会报错。所以分别为每个库定义一个全局变量，rtpdec.c是属于libavformat库，而其它几个c文件属于libavcodec库。本来想用bool类型，但编译错误，只好用uint8_t类型.

  ```
  //avcodec.h
  extern uint8_t avformat_frame_is_normal;
  extern uint8_t avcodec_frame_is_normal;
  
  //rtpdec.c
  uint8_t avformat_frame_is_normal;
  
  //error_resilience.c
  uint8_t avcodec_frame_is_normal;
  ```

  2.修改rtpdec.c文件包含 missed %d packets的地方，这里出现丢包，需作标记。注意要添加大括号

  ```
      if (!has_next_packet(s)){
  		avformat_frame_is_normal = 0;
          av_log(s->ic, AV_LOG_WARNING,
                 "RTP: missed %d packets\n", s->queue->seq - s->seq - 1);
  	}
  ```

  3.修改error_resilience.c文件 包含concealing %d DC, %d AC, %d MV errors in %c frame的地方。这里出现解包错误，需标记

  ```
      avcodec_frame_is_normal = 0;
      av_log(s->avctx, AV_LOG_INFO, "concealing %d DC, %d AC, %d MV errors in %c frame\n",
             dc_error, ac_error, mv_error, av_get_picture_type_char(s->cur_pic.f->pict_type));
  
  ```

  4.修改h264_cavlc.c文件中包含 Invalid level prefix处 这里出错 需标记。查找发现有两处

  ```
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "Invalid level prefix\n");
  ```

    修改h264_cavlc.c文件中包含dquant out of range处，出错，需标记

  ```
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "dquant out of range (%d) at %d %d\n", dquant, sl->mb_x, sl->mb_y);
  ```

    修改h264_cavlc.c文件中包含corrupted macroblock处，出错，需标记

  ```
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "corrupted macroblock %d %d (total_coeff=%d)\n", sl->mb_x, sl->mb_y, total_coeff);
  ```

    修改h264_cavlc.c文件中包含negative number of zero coeffs at处，出错，需标记

  ```
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "negative number of zero coeffs at %d %d\n", sl->mb_x, sl->mb_y);
  ```

    修改h264_cavlc.c文件中包含mb_type %d in %c slice too large at %d %d处，出错，徐标记

  ```
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "mb_type %d in %c slice too large at %d %d\n", mb_type, av_get_picture_type_char(sl->slice_type), sl->mb_x, sl->mb_y);
  ```

    修改h264_cavlc.c文件中包含cbp too large处，出错，需标记  。查找这里也有两处

  ```
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "cbp too large (%u) at %d %d\n", cbp, sl->mb_x, sl->mb_y);
  ```

  5.修改error_resilience.c文件中包含Cannot use previous picture in error concealment处，出错，需标记

  ```
  avcodec_frame_is_normal = 0;
  av_log(s->avctx, AV_LOG_WARNING, "Cannot use previous picture in error concealment\n");
  ```

     修改error_resilience.c文件中包含Cannot use next picture in error concealment处，出错，需标记

  ```
  avcodec_frame_is_normal = 0;
  av_log(s->avctx, AV_LOG_WARNING, "Cannot use next picture in error concealment\n");
  ```

  6.修改h264_parse.c文件中包含out of range intra chroma pred mode处，出错，需标记。（原教程是h264.c，但是没有这个文件，而这些关键log是在h264_parse.c，下面h264_parse.c原教程也是h264.c）

  ```
  avcodec_frame_is_normal = 0;
  av_log(logctx, AV_LOG_ERROR,
  	"out of range intra chroma pred mode\n");
  ```

     修改h264_parse.c文件中包含top block unavailable for requested intra mode处，出错，需标记。查找有两处。

  ```
  avcodec_frame_is_normal = 0;
  av_log(logctx, AV_LOG_ERROR,
  	"top block unavailable for requested intra mode\n");
  ```

     修改h264_parse.c文件中包含left block unavailable for requested intra mode处，出错，需标记

  ```
  avcodec_frame_is_normal = 0;
  av_log(logctx, AV_LOG_ERROR,
  	"left block unavailable for requested intra mode\n");
  ```

  7.修改h264_slice.c文件中包含error while decoding MB处，出错，需标记。查找有两处。

  ```
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR,
  	"error while decoding MB %d %d, bytestream %"PTRDIFF_SPECIFIER"\n",
  	sl->mb_x, sl->mb_y,
  	sl->cabac.bytestream_end - sl->cabac.bytestream);
  	
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR,
      "error while decoding MB %d %d\n", sl->mb_x, sl->mb_y);
  ```

  8.修改svq3.c文件中包含error while decoding MB处，出错，需标记

  ```
  avcodec_frame_is_normal = 0;
  av_log(s->avctx, AV_LOG_ERROR,
  	"error while decoding MB %d %d\n", s->mb_x, s->mb_y);
  ```

  9.每一帧处理后或处理前，都需要将全局变量重置。教程没说哪里进行重置，我不太懂ffmpeg源码，所以自己调用ffmpeg方法进行解码的地方进行重置。不知道为什么，如果我在avcodec_send_packet前面重置，花屏时可以在avcodec_receive_frame之后获取到avcodec_frame_is_normal为0。但是如果我在avcodec_receive_frame前面重置，avcodec_receive_frame之后获取到的avcodec_frame_is_normal一直为1。

  ```
  avcodec_frame_is_normal = 1;
  avformat_frame_is_normal = 1;
  ```

  10.修改完成后，需重新编译ffmpeg源码。可以在使用的时候将全局变量打印出来，会发现一旦有丢包或解码错误现象，全局变量就会有对应的值。这样就可以通过该全局变量来判断该帧是否完整了，从而过滤掉。判断花屏后，需要等待I帧再显示，否则还是有花屏现象。

  ```
  if(avcodec_frame_is_normal == 0 || avformat_frame_is_normal == 0){
      av_frame_unref(frame);
      need_key_frame = true;
      continue;
  }
  if(need_key_frame){
      if (frame->key_frame == 1){
      	need_key_frame = false;
      }else{
      	av_frame_unref(frame);
      	continue;
      }
  }
  ```


- 看到一个教程说视频流如果无法保证视频流是一帧一帧的发送，是要添加CODEC_FLAG_TRUNCATED。参考网址：https://www.oschina.net/question/436610_51323（用ffmpeg中的avcodec_decode_video解码视频流，注释掉CODEC_FLAG_TRUNCATED，有马赛克？）。

  ```
      if (mVideoCodec->capabilities&CODEC_CAP_TRUNCATED) {
          mVideoCodecCtx->flags |= CODEC_FLAG_TRUNCATED;
      }
  ```

- 看到关于花屏的解析的教程，感觉好像挺有用，网址：https://blog.csdn.net/zhoubotong2012/article/details/103002257（解码H264视频出现花屏或马赛克的问题）。通过使用av_parser_parse2函数把多个不连续的数据块组成一帧，而不是使用CODEC_FLAG_TRUNCATED。但是发现av_read_frame源代码有调用av_parser_parse2。

# 9.添加缓存队列，把接收packet和解码分成两个线程

- 队列的实现，自定义循环列表，保存avpacket指针的值，创建头文件AVPacketQueue.h

  ```
  
  //
  // Created by leeson on 2021/9/3.
  //
  /**
   * 参考：数据结构之队列(C语言实现)
   * 网址：https://blog.csdn.net/kang___xi/article/details/53425517
   */
  
  #ifndef OCTANTPANCHIP_AVPACKETQUEUE_H
  #define OCTANTPANCHIP_AVPACKETQUEUE_H
  
  #include <stdio.h>
  
  #include <stdlib.h>
  
  extern "C" {
  #include <libavformat/avformat.h>
  #include <libavcodec/avcodec.h>
  #include <libavutil/opt.h>
  #include <libavutil/imgutils.h>
  #include <libswscale/swscale.h>
  }
  
  #define MAXSIZE 10
  
  
  class AVPacketQueue {
  private:
      AVPacket* elem[MAXSIZE];
      int front;        //下标可取,指向队头元素
      int rear;            //下标不可取,指向元素应放入的位置
  public:
      AVPacketQueue();
      ~AVPacketQueue();
  
      bool Push(AVPacket* val);       //入队，从队尾(rear)入
  
      bool Pop(AVPacket* packet);     //出队，从队首(front)出
  
      int GetQueueLen();         //获取队列长度
  
      bool IsEmpty();             //队列为空则返回true
  
      bool IsFull();                 //队列满则返回false
  };
  
  
  #endif //OCTANTPANCHIP_AVPACKETQUEUE_H
  
  ```

- 创建AVPacketQueue.cpp，实现队列方法，av_packet_alloc创建packet指针，av_copy_packet复制packet指针指向的值，av_packet_free释放packet指针
  $$
  
  $$

  ```
  //
  // Created by leeson on 2021/9/3.
  //
  
  #include "AVPacketQueue.h"
  
  AVPacketQueue::AVPacketQueue(){
      front = 0;
      rear = 0;
      for (int i = 0; i != MAXSIZE; ++i) {
          //elem[i] = nullptr;
          elem[i] = av_packet_alloc();
      }
  }
  
  AVPacketQueue::~AVPacketQueue(){
      front = 0;
      rear = 0;
      for (int i = 0; i != MAXSIZE; ++i) {
          //elem[i] = nullptr;
          av_packet_free(&elem[i]);
      }
  }
  
  //入队，从队尾(rear)入
  bool AVPacketQueue::Push(AVPacket* val){
      //入队之前应该先判断队列是否已经满了
  
      if (IsFull())
  
      {
  
          return false;
  
      }
  
      //elem[rear]= val;               //将元素放入队列，这里也说明front是可取的
      av_copy_packet(elem[rear], val);
  
      rear = (rear+ 1) % MAXSIZE;    //更新队尾位置
  
      return true;
  }
  
  //出队，从队首(front)出
  bool AVPacketQueue::Pop(AVPacket* packet){
  //出队之前应该先判断队列是否是空的
  
      if (IsEmpty())
  
      {
  
          return false;
  
      }
  
      //packet = elem[front];           //取出队头元素
      av_copy_packet(packet, elem[front]);
  
  
      front = (front+ 1) % MAXSIZE;  //更新队头位置
  
      return true;
  }
  
  //获取队列长度
  int AVPacketQueue::GetQueueLen(){
      return ((rear - front + MAXSIZE) % MAXSIZE);
  }
  
  //队列为空则返回true
  bool AVPacketQueue::IsEmpty(){
      return rear == front;
  }
  
  //队列满则返回false
  bool AVPacketQueue::IsFull(){
      return (rear + 1)%MAXSIZE == front;
  }
  
  ```

- 在打开RTSP接收packet的线程中，把packet放入队列。我为了不影响延时，满队列后弹出packet后再放入新的packet

  ```
      while (stopFlag == false) {
          if ((ret = av_read_frame(fmt_ctx, &packet)) < 0)
              break;
          playing = true;
  
          while (queue.IsFull()){
              AVPacket *queuePacket = av_packet_alloc();
              queue.Pop(queuePacket);
              av_packet_free(&queuePacket);
  //            usleep(1000*10);
          }
          LOGI("before queue.Push, len=%d",queue.GetQueueLen());
          queue.Push(&packet);
  
          av_packet_unref(&packet);
      }
  ```

- 在解码packet的线程中，把packet取出来

  ```
  AVPacket *packetPtr = av_packet_alloc();
  
      while (stopFlag == false || playing == true){
          if (queue.IsEmpty()){
              usleep(1000*10);
              continue;
          }
          LOGI("before queue.Pop, len=%d",queue.GetQueueLen());
          int ret = 0;
          if (queue.Pop(packetPtr)){
              if (packetPtr->stream_index == video_stream_index) {
              	//解码
              }
          }
      }
  ```


- 后面测试发现有内存协议，应该是AVPacket free后，里面的data并没有立即释放，所以需要手动释放

  ```
  free(queuePacket->data);
  av_packet_free(&queuePacket);
  ```

- 为了安全，AVPacketQueue添加了互斥锁，分别对读和写添加各自的互斥锁

  ```
      pthread_mutex_t readLock;//读互斥锁
      pthread_mutex_t writeLock;//写互斥锁
      
  AVPacketQueue::AVPacketQueue(){
      ......
      pthread_mutex_init(&readLock,NULL);
      pthread_mutex_init(&writeLock,NULL);
  }
  
  //入队，从队尾(rear)入
  bool AVPacketQueue::Push(AVPacket* val){
      //入队之前应该先判断队列是否已经满了
      pthread_mutex_lock(&writeLock);
      if (IsFull())
      {
          pthread_mutex_unlock(&writeLock);
          return false;
      }
      ......
      pthread_mutex_unlock(&writeLock);
      return true;
  }
  bool AVPacketQueue::Pop(AVPacket* packet){
      pthread_mutex_lock(&readLock);
      if (IsEmpty())
      {
          pthread_mutex_unlock(&readLock);
          return false;
      }
  	......
      pthread_mutex_unlock(&readLock);
      return true;
  }
  ```


# 10.编译多个架构的SO文件

- 参考网址：https://blog.csdn.net/yuxiatongzhi/article/details/80096802（编译Android平台使用的FFmpeg(armeabi,armeabi-v7a,arm64-v8a,x86,x86_64)）

- 修改x264的build.sh，在原来脚本的基础上参考网页教程修改

  ```
  #!/bin/bash
  NDK=/mnt/d/linux/android-ndk-r14b
  
  ANDROID_ARMV5_CFLAGS="-march=armv5te"
  ANDROID_ARMV7_CFLAGS="-march=armv7-a -mfloat-abi=softfp -mfpu=neon"  #-mfloat-abi=hard -mfpu=vfpv3-d16 #-mfloat-abi=hard -mfpu=vfp
  ANDROID_ARMV8_CFLAGS="-march=armv8-a"
  ANDROID_X86_CFLAGS="-march=i686 -mtune=intel -mssse3 -mfpmath=sse -m32"
  ANDROID_X86_64_CFLAGS="-march=x86-64 -msse4.2 -mpopcnt -m64 -mtune=intel"
  
  configure()
  {
      CPU=$1
      PREFIX=$(pwd)/android/$CPU
      HOST=""
      CROSS_PREFIX=""
      SYSROOT=""
      ASM_FLAG=""
      if [ "$CPU" == "armv7-a" ]
      then
          HOST=arm-linux
          SYSROOT=$NDK/platforms/android-21/arch-arm/
          CROSS_PREFIX=$NDK/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-
          CFALGS="$ANDROID_ARMV7_CFLAGS"
  	ASM_FLAG=""
      elif [ "$CPU" == "armeabi" ]
      then
          HOST=arm-linux
          SYSROOT=$NDK/platforms/android-21/arch-arm/
          CROSS_PREFIX=$NDK/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin/arm-linux-androideabi-
          CFALGS="$ANDROID_ARMV5_CFLAGS"
  	ASM_FLAG=--disable-asm
      elif [ "$CPU" == "x86" ]
      then
          HOST=i686-linux
          SYSROOT=$NDK/platforms/android-21/arch-x86/
          CROSS_PREFIX=$NDK/toolchains/x86-4.9/prebuilt/linux-x86_64/bin/i686-linux-android-
          CFALGS="$ANDROID_X86_CFLAGS"
  	ASM_FLAG=--disable-asm
      elif [ "$CPU" == "x86_64" ]
      then
          HOST=x86_64-linux
          SYSROOT=$NDK/platforms/android-21/arch-x86_64/
          CROSS_PREFIX=$NDK/toolchains/x86_64-4.9/prebuilt/linux-x86_64/bin/x86_64-linux-android-
          CFALGS="$ANDROID_X86_64_CFLAGS"
  	ASM_FLAG=--disable-asm
      else
          HOST=aarch64-linux
          SYSROOT=$NDK/platforms/android-21/arch-arm64/
          CROSS_PREFIX=$NDK/toolchains/aarch64-linux-android-4.9/prebuilt/linux-x86_64/bin/aarch64-linux-android-
          CFALGS="$ANDROID_ARMV8_CFLAGS"
  	ASM_FLAG=--disable-asm
      fi
      ./configure \
      $ASM_FLAG \
      --prefix=$PREFIX \
      --host=$HOST \
      --enable-pic \
      --enable-static \
      --enable-neon \
      --extra-cflags="$CFALGS -fPIE -pie" \
      --extra-ldflags="-fPIE -pie" \
      --cross-prefix=$CROSS_PREFIX \
      --sysroot=$SYSROOT
  }
  
  build()
  {
      make clean
      cpu=$1
      echo "build $cpu"
  
      configure $cpu
      #-j<CPU核心数>
      make -j4
      make install
  }
  build armeabi
  build x86
  build x86_64
  build arm64
  build armv7-a
  
  ```

  

- 编译发现报错说asm有问题，所以加上--disable-asm

- 修改FFmpeg的build.sh，在原来脚本的基础上参考网页教程修改

  ```
  #!/bin/bash
  NDK=/mnt/d/linux/android-ndk-r19c
  TOOLCHAIN=$NDK/toolchains/llvm/prebuilt/linux-x86_64
  API=21
  ADDI_CFLAGS="-fPIE -pie"
  ADDI_LDFLAGS="-fPIE -pie"
  
  ANDROID_ARMV5_CFLAGS="-march=armv5te"
  ANDROID_ARMV7_CFLAGS="-march=armv7-a -mfloat-abi=softfp -mfpu=neon"  #-mfloat-abi=hard -mfpu=vfpv3-d16 #-mfloat-abi=hard -mfpu=vfp
  ANDROID_ARMV8_CFLAGS="-march=armv8-a"
  ANDROID_X86_CFLAGS="-march=i686 -mtune=intel -mssse3 -mfpmath=sse -m32"
  ANDROID_X86_64_CFLAGS="-march=x86-64 -msse4.2 -mpopcnt -m64 -mtune=intel"
  
  configure()
  {
      CPU=$1
      PREFIX=$(pwd)/android/$CPU
      x264=$(pwd)/../x264-master/android/$CPU
      CROSS_PREFIX=""
      SYSROOT=""
      ARCH=""
      CFALGS=""
      ASM_FLAG=""
      if [ "$CPU" == "armv7-a" ]
      then
          ARCH="arm"
          TOOL_CPU_NAME=armv7a
          SYSROOT=$NDK/toolchains/llvm/prebuilt/linux-x86_64/sysroot
          TOOL_PREFIX=$TOOLCHAIN/bin/$TOOL_CPU_NAME-linux-androideabi
          CC=$TOOL_PREFIX$API-clang
          CXX=$TOOL_PREFIX$API-clang++
          CROSS_PREFIX=$TOOLCHAIN/bin/arm-linux-androideabi-
  	CFALGS="$ANDROID_ARMV7_CFLAGS"
  	ASM_FLAG=""
      elif [ "$CPU" == "armeabi" ]
      then
      	ARCH="arm"
  	TOOL_CPU_NAME=arm
  	SYSROOT=$NDK/toolchains/llvm/prebuilt/linux-x86_64/sysroot
  	TOOL_PREFIX=$TOOLCHAIN/bin/$TOOL_CPU_NAME-linux-androideabi
  	CC=$TOOL_PREFIX-clang
  	CXX=$TOOL_PREFIX-clang++
  	CROSS_PREFIX=$TOOLCHAIN/bin/arm-linux-androideabi-
  	CFALGS="$ANDROID_ARMV5_CFLAGS"
  	ASM_FLAG=--disable-asm
      elif [ "$CPU" == "x86" ]
      then
  	ARCH="x86"
  	TOOL_CPU_NAME=i686
  	SYSROOT=$NDK/toolchains/llvm/prebuilt/linux-x86_64/sysroot
  	TOOL_PREFIX=$TOOLCHAIN/bin/$TOOL_CPU_NAME-linux-android
  	CC=$TOOL_PREFIX$API-clang
  	CXX=$TOOL_PREFIX$API-clang++
  	CROSS_PREFIX=$TOOLCHAIN/bin/i686-linux-android-
  	CFALGS="$ANDROID_X86_CFLAGS"
  	ASM_FLAG=--disable-asm
      elif [ "$CPU" == "x86_64" ]
      then
  	ARCH="x86_64"
  	TOOL_CPU_NAME=x86_64
  	SYSROOT=$NDK/toolchains/llvm/prebuilt/linux-x86_64/sysroot
  	TOOL_PREFIX=$TOOLCHAIN/bin/$TOOL_CPU_NAME-linux-android
  	CC=$TOOL_PREFIX$API-clang
  	CXX=$TOOL_PREFIX$API-clang++
  	CROSS_PREFIX=$TOOLCHAIN/bin/x86_64-linux-android-
  	CFALGS="$ANDROID_X86_64_CFLAGS"
  	ASM_FLAG=--disable-asm
      else
          ARCH="arm64"
          TOOL_CPU_NAME=aarch64
          SYSROOT=$NDK/toolchains/llvm/prebuilt/linux-x86_64/sysroot
          TOOL_PREFIX=$TOOLCHAIN/bin/$TOOL_CPU_NAME-linux-android
          CC=$TOOL_PREFIX$API-clang
          CXX=$TOOL_PREFIX$API-clang++
          CROSS_PREFIX=$TOOLCHAIN/bin/aarch64-linux-android-
  	CFALGS="$ANDROID_ARMV8_CFLAGS"
  	ASM_FLAG=--disable-asm
      fi
      ./configure \
      $ASM_FLAG \
      --prefix=$PREFIX \
      --disable-avdevice \
      --disable-static \
      --disable-doc \
      --disable-ffplay \
      --disable-doc \
      --disable-symver \
      --enable-neon \
      --enable-shared \
      --enable-libx264 \
      --enable-gpl \
      --enable-pic \
      --enable-jni \
      --enable-pthreads \
      --enable-mediacodec \
      --enable-encoder=aac \
      --enable-encoder=gif \
      --enable-encoder=libopenjpeg \
      --enable-encoder=libmp3lame \
      --enable-encoder=libwavpack \
      --enable-encoder=libx264 \
      --enable-encoder=mpeg4 \
      --enable-encoder=pcm_s16le \
      --enable-encoder=png \
      --enable-encoder=srt \
      --enable-encoder=subrip \
      --enable-encoder=yuv4 \
      --enable-encoder=text \
      --enable-decoder=aac \
      --enable-decoder=aac_latm \
      --enable-decoder=libopenjpeg \
      --enable-decoder=mp3 \
      --enable-decoder=mpeg4_mediacodec \
      --enable-decoder=pcm_s16le \
      --enable-decoder=flac \
      --enable-decoder=flv \
      --enable-decoder=gif \
      --enable-decoder=png \
      --enable-decoder=srt \
      --enable-decoder=xsub \
      --enable-decoder=yuv4 \
      --enable-decoder=vp8_mediacodec \
      --enable-decoder=h264_mediacodec \
      --enable-decoder=hevc_mediacodec \
      --enable-hwaccel=h264_mediacodec \
      --enable-hwaccel=mpeg4_mediacodec \
      --enable-ffmpeg \
      --enable-bsf=aac_adtstoasc \
      --enable-bsf=h264_mp4toannexb \
      --enable-bsf=hevc_mp4toannexb \
      --enable-bsf=mpeg4_unpack_bframes \
      --enable-cross-compile \
      --cross-prefix=$CROSS_PREFIX \
      --target-os=android \
      --arch=$ARCH \
      --cc=$CC \
      --cxx=$CXX \
      --sysroot=$SYSROOT \
      --extra-cflags="$CFALGS -I$x264/include $ADDI_CFLAGS" \
      --extra-ldflags="-L$x264/lib"
  }
  
  build()
  {
      make clean
      cpu=$1
      echo "build $cpu"
      
      configure $cpu
      make -j4
      make install
  }
  
  build armeabi
  build x86
  build x86_64
  build arm64
  build armv7-a
  
  ```

  

