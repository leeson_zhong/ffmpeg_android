package com.octant.testffmpeg;

import android.os.Environment;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class TcpConnectThread extends Thread {

    private Socket clientSocket = null;
    private String Server_IP = "192.168.25.1";
    private int port = 8081;
    private byte[] senddata_a;

    String filePath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/eSky/";
    String fileName = null;

    private static boolean startRun = true;
    private static TcpConnectThread instance = new TcpConnectThread();

    private TcpConnectThread(){
    }

    public static TcpConnectThread getInstance(){
        if (startRun) {
            startRun = false;
            instance.start();
        }
        return instance;
    }

    @Override
    public void run() {
        super.run();
        while (true){

            if (null == clientSocket) {
                clientSocket = new Socket();
            }
            if (false ==clientSocket.isConnected()) {
                try{
                    clientSocket.connect(new InetSocketAddress(InetAddress.getByName(Server_IP),port),10);
                }catch(Exception e){
                    //當斷線時會跳到catch,可以在這裡寫上斷開連線後的處理
                    //e.printStackTrace();
                    //Log.e("text","Socket連線="+e.toString()+",ip="+Server_IP);
//                    if (fileName==null){
//                        fileName = "log_"+System.currentTimeMillis()+".txt";
//                    }
//                    writeTxtToFile(System.currentTimeMillis()+" cannot connect tcp", filePath, fileName);
                    if (clientSocket != null){
                        try {
                            clientSocket.close();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                    clientSocket = null;
                    SendData.getInstance().setConnect(false);
                    ReceiveData.getInstance().setFlightVersion(-1);
                    ReceiveData.getInstance().setCameraVersion(-1);
                    ReceiveData.getInstance().rect_udp.setEmpty();
                }
            }

            if (clientSocket!= null && clientSocket.isConnected()) {
                try {

                    DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());


                    senddata_a = SendData.getInstance().packSendData();

                    out.write(senddata_a);
                    //Log.i("==leeson","data="+ Arrays.toString(senddata_a));
                    SendData.getInstance().setConnect(true);

                    DataInputStream in = new DataInputStream(clientSocket.getInputStream());
                    byte[] receiveData = new byte[1024];
                    int len = in.read(receiveData);
                    if (len > 0){
                    }

                } catch (Exception e) {
//                    if (fileName==null){
//                        fileName = "log_"+System.currentTimeMillis()+".txt";
//                    }
//                    writeTxtToFile(System.currentTimeMillis()+" send or read tcp data failed", filePath, fileName);
                    if (clientSocket != null){
                        try {
                            clientSocket.close();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                    clientSocket = null;
                    SendData.getInstance().setConnect(false);
                    ReceiveData.getInstance().setCameraVersion(-1);
                    ReceiveData.getInstance().setFlightVersion(-1);
                    ReceiveData.getInstance().rect_udp.setEmpty();
                    e.printStackTrace();
                }
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    // 将字符串写入到文本文件中
    private void writeTxtToFile(String strcontent, String filePath, String fileName) {
        //生成文件夹之后，再生成文件，不然会出错
        makeFilePath(filePath, fileName);

        String strFilePath = filePath + fileName;
        // 每次写入时，都换行写
        String strContent = strcontent + "\r\n";
        try {
            File file = new File(strFilePath);
            if (!file.exists()) {
                Log.d("TestFile", "Create the file:" + strFilePath);
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            RandomAccessFile raf = new RandomAccessFile(file, "rwd");
            raf.seek(file.length());
            raf.write(strContent.getBytes());
            raf.close();
        } catch (Exception e) {
            Log.e("TestFile", "Error on write File:" + e);
        }
    }

//生成文件

    private File makeFilePath(String filePath, String fileName) {
        File file = null;
        makeRootDirectory(filePath);
        try {
            file = new File(filePath + fileName);
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

//生成文件夹

    private static void makeRootDirectory(String filePath) {
        File file = null;
        try {
            file = new File(filePath);
            if (!file.exists()) {
                file.mkdir();
            }
        } catch (Exception e) {
            Log.i("error:", e + "");
        }
    }
}
