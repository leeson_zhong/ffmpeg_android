package com.octant.testffmpeg;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback{

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    boolean surfaceCreatedFlag = false,writePermittedFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        TextView tv = findViewById(R.id.sample_text);
        tv.setText(stringFromJNI());

        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceHolder = surfaceView.getHolder();

        surfaceHolder.addCallback(this);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            int writePermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (writePermission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};

                int permsRequestCode = 200;

                requestPermissions(perms, permsRequestCode);
            }else {
                writePermittedFlag = true;
            }
        }else {
            writePermittedFlag = true;
        }

        playVideoThread.start();
        TcpConnectThread.getInstance().start();

    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
    public native void playWithSurface(String videopath, Object surface);
    public native void playRtspWithSurface(String videopath, Object surface);

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        surfaceCreatedFlag = true;
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (surfaceHolder != null) {
            surfaceHolder = null;
        }
    }

    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                boolean writeAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (false == writeAccepted) {
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                        String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE"};

                        requestPermissions(perms, permsRequestCode);
                    }
                }else {
                    writePermittedFlag = true;
                }

                break;
        }
    }


    Thread playVideoThread = new Thread(new Runnable() {
        @Override
        public void run() {
            while (surfaceCreatedFlag == false || writePermittedFlag == false){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
//            String path = Environment.getExternalStorageDirectory().getAbsolutePath();
//            File dir = new File(path);
//            if (!dir.exists()) {
//                dir.mkdirs();
//            }
//            final String inputPath = path + "/MOVI0001.mov";
//            playWithSurface(inputPath,surfaceHolder.getSurface());
            while(true){
                String url = "rtsp://192.168.25.1:8080/?action=stream";
                playRtspWithSurface(url,surfaceHolder.getSurface());
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    });
}
