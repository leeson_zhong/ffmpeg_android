package com.octant.testffmpeg;

import android.graphics.Rect;

/**
 * Created by linsheng on 2018/5/30.
 */

public class ReceiveData {

    private static ReceiveData instance=new ReceiveData();//在自己内部定义自己的一个实例
    private ReceiveData(){}
    public static ReceiveData getInstance(){         //此静态方法供外部直接访问
        return instance;
    }

    private int fun;
    private boolean calibration = false;//fun&0x01 正在降准
    private boolean angle = false;//fun&0x02 角度过大
    private boolean batteryLow = false;//fun&0x04 低电
    private boolean flying = false;//fun&0x08 空中飞行
    private boolean calibrationNeeded = false;//fun&0x20 需要校准，更新飞控固件后，第一次飞行前需要校准
    private int flowState;
    private int faceDetect;
    private int droneIdentify = 2;
    private int height;
    private int speed;
    private int battery;
    private int flightVersion = -1;
    private int cameraVersion = -1;

    private boolean saveInSD = false;
    private boolean recording = false;

    public Rect rect_udp=new Rect(0,0,0,0);
    public Rect rect_P1_udp=new Rect(0,0,0,0);
    public Rect rect_P2_udp=new Rect(0,0,0,0);
    public Rect rect_F_udp=new Rect(0,0,0,0);

    public int getFun() {
        return fun;
    }

    public boolean isCalibration() {
        return calibration;
    }

    public boolean isAngle() {
        return angle;
    }

    public boolean isBatteryLow() {
        return batteryLow;
    }

    public boolean isFlying() {
        return flying;
    }

    public boolean isCalibrationNeeded() {
        return calibrationNeeded;
    }

    public int getFlowState() {
        return flowState;
    }

    public int getFaceDetect() {
        return faceDetect;
    }

    public int getDroneIdentify() {
        return droneIdentify;
    }

    public int getHeight() {
        return height;
    }

    public int getSpeed() {
        return speed;
    }

    public int getBattery() {
        return battery;
    }

    public int getFlightVersion() {
        return flightVersion;
    }

    public void setFlightVersion(int flightVersion) {
        this.flightVersion = flightVersion;
    }

    public int getCameraVersion() {
        return cameraVersion;
    }

    public void setCameraVersion(int cameraVersion) {
        this.cameraVersion = cameraVersion;
    }

    public boolean isSaveInSD() {
        return saveInSD;
    }

    public void setSaveInSD(boolean saveInSD) {
        this.saveInSD = saveInSD;
    }

    public boolean isRecording() {
        return recording;
    }

    public void setRecording(boolean recording) {
        this.recording = recording;
    }

    public boolean setRxData(byte[] rxData){
        if (rxData.length!=11){
            return false;
        }
        if ((rxData[0] & 0xff) !=0x77 || (rxData[10] & 0xff) !=0x88){
            return false;
        }

        fun = rxData[1]&0xff;
        if ((fun&0x01)==0){
            calibration = false;
        }else {
            calibration = true;
        }
        if ((fun&0x02)==0){
            angle = false;
        }else {
            angle = true;
        }
        if ((fun&0x04)==0){
            batteryLow = false;
        }else {
            batteryLow = true;
        }
        if ((fun&0x08)==0){
            flying = false;
        }else {
            flying = true;
        }
        if ((fun&0x20)==0){
            calibrationNeeded = false;
        }else {
            calibrationNeeded = true;
        }
        flowState = rxData[2]&0xff;
        faceDetect = rxData[3]&0xff;
        droneIdentify = rxData[4]&0xff;
        height = rxData[5] & 0xff;
        speed = rxData[6] & 0xff;
        battery = rxData[7] & 0xff;
        flightVersion = rxData[8] & 0xff;
        return true;
    }
}
