package com.octant.testffmpeg;

/**
 * Created by linsheng on 2018/5/25.
 */

public class SendData {

    private static SendData instance=new SendData();//在自己内部定义自己的一个实例
    private SendData(){}
    public static SendData getInstance(){         //此静态方法供外部直接访问
        return instance;
    }

    private boolean connect = false;
    private boolean takePhoto = false;
    private boolean recordVideo = false;

    private int throttle = 127;//0-255
    private int yaw= 127;//0-255
    private int roll= 127;//0-255
    private int pitch= 127;//0-255
    private boolean calibration = false;//0x01
    private boolean takeoff = false;//0x02
    private boolean land = false;//0x04
    private boolean disarm = false;//0x08
    private boolean facetrack1 = false;//0x20
    //private boolean toss = false;//0x40
    private byte autoModeSelect;//Auto模式选择:1为自动拍照. 2为自动录像. 3.为环绕. 4.为自转拍照. 5.为自转录像.
    private byte autoPhotoCount;//自动模式下的照片数量
    private byte speed = 0;// 0: indoor mode, 1: outdoor mode
    public String logStr;
    private int count = 1;

    public boolean isConnect() {
        return connect;
    }

    public void setTakePhoto(boolean takePhoto) {
        this.takePhoto = takePhoto;
    }

    public void setRecordVideo(boolean recordVideo) {
        this.recordVideo = recordVideo;
    }

    public void setConnect(boolean connect) {
        this.connect = connect;
    }

    public void setThrottle(int throttle) {
        this.throttle = throttle;
    }

    public void setYaw(int yaw) {
        this.yaw = yaw;
    }

    public void setRoll(int roll) {
        this.roll = roll;
    }

    public void setPitch(int pitch) {
        this.pitch = pitch;
    }

    public void setCalibration(boolean calibration) {
        this.calibration = calibration;
    }

    public void setTakeoff(boolean takeoff) {
        this.takeoff = takeoff;
    }

    public void setLand(boolean land) {
        this.land = land;
    }

    public void setDisarm(boolean disarm) {
        this.disarm = disarm;
    }

    public boolean isFacetrack1() {
        return facetrack1;
    }

    public void setFacetrack1(boolean facetrack1) {
        this.facetrack1 = facetrack1;
    }

    public byte getAutoModeSelect() {
        return autoModeSelect;
    }

    public void setAutoModeSelect(byte autoModeSelect) {
        this.autoModeSelect = autoModeSelect;
    }

    public byte getAutoPhotoCount() {
        return autoPhotoCount;
    }

    public void setAutoPhotoCount(byte autoPhotoCount) {
        this.autoPhotoCount = autoPhotoCount;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public byte getSpeed() {
        return speed;
    }

    public void setSpeed(byte speed) {
        this.speed = speed;
    }

    private int setRangeValue(int value, int min, int max){
        if (value<min){
            return min;
        }else if (value > max){
            return max;
        }else {
            return value;
        }
    }

    private byte[] getTxData(){
        byte data[] = new byte[13];
        data[0] = (byte) setRangeValue(throttle,0,255);
        data[1] = (byte) setRangeValue(yaw,0,255);
        data[2] = (byte) setRangeValue(roll,0,255);
        data[3] = (byte) setRangeValue(pitch,0,255);
        data[4] = 0;
        if (calibration){
            data[4] |= 0x01;
        }
        if (takeoff){
            data[4] |= 0x02;
        }
        if (land){
            data[4] |= 0x04;
        }
        if (disarm){
            data[4] |= 0x08;
        }
        if (facetrack1){
            data[4] |= 0x20;
        }
        data[5] = autoModeSelect;
        data[6] = (byte) 0x01;
        data[7] = autoPhotoCount;
        data[8] = 100;
        data[9] = speed;
        data[10] = 0;
        data[11] = (byte)count;
        count ++;
        if (count>100){
            count = 1;
        }
//        logStr = "";
        byte checksum = 0x5a;
        for (int i=0; i<12; i++){
            checksum ^= data[i];
//            logStr += Integer.toHexString(data[i])+",";
        }
        data[12] = checksum;
//        logStr += Integer.toHexString(data[12]);
//        logStr += Integer.toHexString(data[5])+",";
//        logStr += Integer.toHexString(data[1])+",";
//        logStr += Integer.toHexString(data[2])+",";
//        logStr += Integer.toHexString(data[7]);
//        Log.i("==leeson","getNewTxData="+logStr);
        return data;

    }

    //cmd=0=takePhoto,cmd=1=RecordVideo
    private byte[] getCmdData(int cmd){
        byte[] a=new byte[13];
        a[0]= (byte)244;
        a[2]= (byte)244;
        a[1]=17;
        a[3]=(byte)244;
        a[4]=(byte)244;
        a[5]= 17;
        a[6]=0x01;
        a[7]=0x00;
        a[8]=0x00;
        a[9]=0x00;
        a[10]=0x00;
        a[11]=0x00;
        switch(cmd) {
            case 0:
                a[6] |= 1<<7;
                a[8]=0x16;
                break;
            case 1:
                a[6] |= 1<<7;
                a[8]=0x19;
                break;
        }

        a[12]= (byte) (0x5a^a[0]^a[1]^a[2]^a[3]^a[4]^a[5]^a[6]^a[7]^a[8]^a[9]^a[10]^a[11]);

        return a;
    }

    public byte[] packSendData() {
        byte[] a = new byte[27];
        a[0] = 'G';  //tag[0]
        a[1] = 'P';
        a[2] = 'S';
        a[3] = 'O';
        a[4] = 'C';
        a[5] = 'K';
        a[6] = 'E';
        a[7] = 'T';//tag[7]
        a[8] = 1;//type[0]
        a[9] = 0; //type[1]
        a[10] = (byte) 0xFF;//mode[0]
        a[11] = 0;//cmd[0
        a[12] = 0;
        a[13] = 0;
        if (takePhoto){
            System.arraycopy(getCmdData(0), 0, a, 14, 13);
            takePhoto = false;
        }else if (recordVideo){
            System.arraycopy(getCmdData(1), 0, a, 14, 13);
            recordVideo = false;
        }else {
            System.arraycopy(getTxData(), 0, a, 14, 13);
        }

        return a;
    }



}
