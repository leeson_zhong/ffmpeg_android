#include <jni.h>
#include <string>

#include <android/log.h>

//#include "include/libavformat/avformat.h"
//#include "include/libavcodec/avcodec.h"
//#include "include/libavutil/opt.h"
//#include "include/libavutil/imgutils.h"
//#include "include/libswscale/swscale.h"
//#include "include/libavcodec/jni.h"
//#include "include/libavutil/frame.h"

#include <android/native_window.h>
#include <android/native_window_jni.h>

extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
#include "libavcodec/jni.h"
}

const char* TAG = "native-lib,leeson,log";

#define ALOGE(fmt, ...) __android_log_vprint(ANDROID_LOG_ERROR, TAG, fmt, ##__VA_ARGS__)
#define ALOGI(fmt, ...) __android_log_vprint(ANDROID_LOG_INFO, TAG, fmt, ##__VA_ARGS__)
#define ALOGD(fmt, ...) __android_log_vprint(ANDROID_LOG_DEBUG, TAG, fmt, ##__VA_ARGS__)
#define ALOGW(fmt, ...) __android_log_vprint(ANDROID_LOG_WARN, TAG, fmt, ##__VA_ARGS__)
#define ALOGV(fmt, ...) __android_log_vprint(ANDROID_LOG_VERBOSE, TAG, fmt, ##__VA_ARGS__)

jboolean FFMPEG_ANDROID_DEBUG = 0;

static void ffmpeg_android_log_callback(void *ptr, int level, const char *fmt, va_list vl){
    if (FFMPEG_ANDROID_DEBUG){
        switch(level) {
            case AV_LOG_DEBUG:
                ALOGD(fmt, vl);
                break;
            case AV_LOG_VERBOSE:
                ALOGV(fmt, vl);
                break;
            case AV_LOG_INFO:
                ALOGI(fmt, vl);
                break;
            case AV_LOG_WARNING:
                ALOGW(fmt, vl);
                break;
            case AV_LOG_ERROR:
                ALOGE(fmt, vl);
                break;
        }
    }
}

extern "C"
JNIEXPORT
jint JNI_OnLoad(JavaVM *vm, void *res) {
    av_jni_set_java_vm(vm, res);
    // 返回jni版本
    return JNI_VERSION_1_6;
}


extern "C" JNIEXPORT jstring JNICALL
Java_com_octant_testffmpeg_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    av_register_all();
    return env->NewStringUTF(hello.c_str());
}

int decodeVideo(const char * videoPath,JNIEnv *env);
ANativeWindow *nativeWindow;
extern "C" void
Java_com_octant_testffmpeg_MainActivity_playWithSurface(
        JNIEnv *env,
        jobject,
        jstring videopath,
        jobject surface) {

    FFMPEG_ANDROID_DEBUG = 1;
    av_log_set_callback(ffmpeg_android_log_callback);
    const char *videoPath = env->GetStringUTFChars(videopath, JNI_FALSE);
    nativeWindow = ANativeWindow_fromSurface(env,surface);
    decodeVideo(videoPath,env);
}

AVFormatContext *fmt_ctx;
AVCodecContext *dec_ctx;
int video_stream_index = -1;
int open_input_file(const char *filename)
{
    int ret;
    AVCodec *dec;

    if ((ret = avformat_open_input(&fmt_ctx, filename, NULL, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open input file\n");
        return ret;
    }

    if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
        return ret;
    }

    /* select the video stream */
    ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find a video stream in the input file\n");
        return ret;
    }
    video_stream_index = ret;

    //使用硬解码。原来是因为编译只启动h264_mediacodec等硬解码器，h264等软解码器没有启动，所以默认是硬解码。
    //编译加上软解码器后，默认是软解码器的，所以要调用硬解码器。
   switch (dec->id){

        case AV_CODEC_ID_H264:{
            dec = avcodec_find_decoder_by_name("h264_mediacodec");
            break;
        }

        case AV_CODEC_ID_MPEG4:{
            dec = avcodec_find_decoder_by_name("mpeg4_mediacodec");
            break;
        }

        default:{
            break;
        }

    }
    /* create decoding context */
    dec_ctx = avcodec_alloc_context3(dec);
    if (!dec_ctx)
        return AVERROR(ENOMEM);

    avcodec_parameters_to_context(dec_ctx, fmt_ctx->streams[video_stream_index]->codecpar);
    av_opt_set_int(dec_ctx, "refcounted_frames", 1, 0);

    /* init the video decoder */
    if ((ret = avcodec_open2(dec_ctx, dec, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
        return ret;
    }

    av_log(NULL, AV_LOG_INFO, "dec_ctx->framerate.num=%d,den=%d,dec_ctx->time_base.num=%d,den=%d\n",dec_ctx->framerate.num,dec_ctx->framerate.den,dec_ctx->time_base.num,dec_ctx->time_base.den);
    __android_log_print(ANDROID_LOG_INFO, TAG,"decode codec name=%s",dec->name);
    return 0;
}


int decodeVideo(const char * videoPath,JNIEnv *env){
    int ret;
    AVPacket packet;
    AVFrame *frame = av_frame_alloc();
    if (!frame ) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
        return -1;
    }
    av_register_all();

    SwsContext *img_convert_ctx;
    AVFrame *pRgbFrame;
    size_t rgbSize;
    uint8_t *pRgbBuff;

    if ((ret = open_input_file(videoPath)) < 0)
        goto end;

    img_convert_ctx = sws_getContext(dec_ctx->width, dec_ctx->height, dec_ctx->pix_fmt, dec_ctx->width, dec_ctx->height, AV_PIX_FMT_RGBA, SWS_FAST_BILINEAR, NULL, NULL, NULL);

    pRgbFrame = av_frame_alloc();
    rgbSize = (size_t) av_image_get_buffer_size(AV_PIX_FMT_RGBA, dec_ctx->width, dec_ctx->height, 1);
    pRgbBuff= (uint8_t *) (av_malloc(rgbSize));
    av_image_fill_arrays(pRgbFrame->data, pRgbFrame->linesize, pRgbBuff, AV_PIX_FMT_RGBA, dec_ctx->width, dec_ctx->height,1);

    void *addr_pixels;
    ANativeWindow_setBuffersGeometry(nativeWindow, dec_ctx->width, dec_ctx->height,
                                     WINDOW_FORMAT_RGBA_8888);
    ANativeWindow_Buffer windowBuffer;
    /* read all packets */
    while (1) {
        if ((ret = av_read_frame(fmt_ctx, &packet)) < 0)
            break;

        if (packet.stream_index == video_stream_index) {
            ret = avcodec_send_packet(dec_ctx, &packet);
            if (ret < 0) {
                av_log(NULL, AV_LOG_ERROR, "Error while sending a packet to the decoder\n");
                break;
            }

            while (ret >= 0) {
                ret = avcodec_receive_frame(dec_ctx, frame);
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                    break;
                } else if (ret < 0) {
                    av_log(NULL, AV_LOG_ERROR, "Error while receiving a frame from the decoder\n");
                    goto end;
                }

                if (ret >= 0) {
                    av_log(NULL, AV_LOG_INFO, "decode a frame success\n");
                    //使用ffmpeg的yuv转换rgb24方法耗时太久，需要差不多100ms
                    //我改成转换成rgba,转换方法使用SWS_BILINEAR，耗时不久，要十几毫秒，如果SWS_FAST_BILINEAR好像更快一点
                    sws_scale(img_convert_ctx, (const uint8_t *const *) frame->data, frame->linesize, 0, dec_ctx->height, pRgbFrame->data, pRgbFrame->linesize);

                    ANativeWindow_lock(nativeWindow, &windowBuffer, 0);

                    //src.data ： rgba的数据
                    //把src.data 一行一行的拷贝到 buffer.bits 里去
                    //填充rgb数据给dst_data
                    uint8_t *dst_data = static_cast<uint8_t *>(windowBuffer.bits);
                    /* 好像如果不是rgba8888，需要一行一行拷贝
                    //stride : 一行多少个数据 （RGBA） * 4
                      int dst_line_size = windowBuffer.stride * 4;

                      //一行一行拷贝
                      for (int i = 0; i < windowBuffer.height; ++i) {
                          //void *memcpy(void *dest, const void *src, size_t n);
                          //从源src所指的内存地址的起始位置开始拷贝n个字节到目标dest所指的内存地址的起始位置中
                          memcpy(dst_data + i * dst_line_size,
                                 rgbaBuff + i * windowBuffer.width * 4, dst_line_size);
                      }*/
                    memcpy(dst_data,pRgbBuff,dec_ctx->width*dec_ctx->height*4);
                    ANativeWindow_unlockAndPost(nativeWindow);


                    av_frame_unref(frame);
                }
            }
        }
        av_packet_unref(&packet);
    }
    end:
    avcodec_free_context(&dec_ctx);
    avformat_close_input(&fmt_ctx);
    av_frame_free(&frame);
    return 0;
}

extern "C" void
Java_com_octant_testffmpeg_MainActivity_playRtspWithSurface(
        JNIEnv *env,
        jobject,
        jstring videopath,
        jobject surface) {

    FFMPEG_ANDROID_DEBUG = 1;
    av_log_set_callback(ffmpeg_android_log_callback);
    const char *videoPath = env->GetStringUTFChars(videopath, JNI_FALSE);

    nativeWindow = ANativeWindow_fromSurface(env,surface);
    int ret;
    AVPacket packet;
    AVFrame *frame = av_frame_alloc();
    if (!frame ) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
        return ;
    }
    av_register_all();
    avformat_network_init();

    SwsContext *img_convert_ctx;
    AVFrame *pRgbFrame;
    size_t rgbSize;
    uint8_t *pRgbBuff;

    AVDictionary* options = NULL;
    //av_dict_set(&options, "buffer_size", "1024000", 0);
    av_dict_set(&options, "max_delay", "000", 0);
    av_dict_set(&options, "stimeout", "2000", 0);  //设置超时断开连接时间
    av_dict_set(&options, "rtsp_transport", "tcp", 0);  //以udp方式打开，如果以tcp方式打开将udp替换为tcp
    AVCodec *dec;

    if ((ret = avformat_open_input(&fmt_ctx, videoPath, NULL,&options)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open input stream,error code=%ld\n",AVERROR(ret));
        return ;
    }

    if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
        return;
    }

    /* select the video stream */
    ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find a video stream in the input file\n");
        return ;
    }
    video_stream_index = ret;

    //默认应该是软解码器，为了保证使用软解码器，使用avcodec_find_decoder_by_name获取软解码
    switch (dec->id){

        case AV_CODEC_ID_H264:{
            dec = avcodec_find_decoder_by_name("h264");
            break;
        }

        case AV_CODEC_ID_MPEG4:{
            dec = avcodec_find_decoder_by_name("mpeg4");
            break;
        }

        default:{
            break;
        }

    }

    /* create decoding context */
    dec_ctx = avcodec_alloc_context3(dec);
    if (!dec_ctx)
        return ;

    avcodec_parameters_to_context(dec_ctx, fmt_ctx->streams[video_stream_index]->codecpar);
    av_opt_set_int(dec_ctx, "refcounted_frames", 1, 0);
   // dec_ctx->flags |= AV_CODEC_FLAG_LOW_DELAY;
    //av_opt_set(dec_ctx->priv_data, "tune", "zerolatency", 0);

    /* init the video decoder */
    if ((ret = avcodec_open2(dec_ctx, dec, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
        return ;
    }

    img_convert_ctx = sws_getContext(dec_ctx->width, dec_ctx->height, dec_ctx->pix_fmt, dec_ctx->width, dec_ctx->height, AV_PIX_FMT_RGBA, SWS_FAST_BILINEAR, NULL, NULL, NULL);

    pRgbFrame = av_frame_alloc();
    rgbSize = (size_t) av_image_get_buffer_size(AV_PIX_FMT_RGBA, dec_ctx->width, dec_ctx->height, 1);
    pRgbBuff= (uint8_t *) (av_malloc(rgbSize));
    av_image_fill_arrays(pRgbFrame->data, pRgbFrame->linesize, pRgbBuff, AV_PIX_FMT_RGBA, dec_ctx->width, dec_ctx->height,1);

    void *addr_pixels;
    ANativeWindow_setBuffersGeometry(nativeWindow, dec_ctx->width, dec_ctx->height,
                                     WINDOW_FORMAT_RGBA_8888);
    ANativeWindow_Buffer windowBuffer;
    /* read all packets */
    while (1) {
        if ((ret = av_read_frame(fmt_ctx, &packet)) < 0)
            break;

        if (packet.stream_index == video_stream_index) {
            av_log(NULL, AV_LOG_INFO, "avcodec_send_packet\n");
            ret = avcodec_send_packet(dec_ctx, &packet);
            if (ret < 0) {
                av_log(NULL, AV_LOG_ERROR, "Error while sending a packet to the decoder\n");
                break;
            }

            while (ret >= 0) {
                ret = avcodec_receive_frame(dec_ctx, frame);
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                    break;
                } else if (ret < 0) {
                    av_log(NULL, AV_LOG_ERROR, "Error while receiving a frame from the decoder\n");
                    goto end;
                }

                if (ret >= 0) {
                    av_log(NULL, AV_LOG_INFO, "decode a frame success\n");
                    //使用ffmpeg的yuv转换rgb24方法耗时太久，需要差不多100ms
                    //我改成转换成rgba,转换方法使用SWS_BILINEAR，耗时不久，要十几毫秒，如果SWS_FAST_BILINEAR好像更快一点
                    sws_scale(img_convert_ctx, (const uint8_t *const *) frame->data, frame->linesize, 0, dec_ctx->height, pRgbFrame->data, pRgbFrame->linesize);
                    av_log(NULL, AV_LOG_INFO, "sws_scale a frame success\n");
                    ANativeWindow_lock(nativeWindow, &windowBuffer, 0);

                    uint8_t *dst_data = static_cast<uint8_t *>(windowBuffer.bits);

                    memcpy(dst_data,pRgbBuff,dec_ctx->width*dec_ctx->height*4);
                    ANativeWindow_unlockAndPost(nativeWindow);

                    av_log(NULL, AV_LOG_INFO, "show a frame success\n");

                    av_frame_unref(frame);
                }
            }
        }
        av_packet_unref(&packet);
    }
    end:
    avcodec_free_context(&dec_ctx);
    avformat_close_input(&fmt_ctx);
    av_frame_free(&frame);
}


